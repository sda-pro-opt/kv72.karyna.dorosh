#include<string>
#include <fstream>	
#include <iostream>
#include <map>
#include <list>
#include <algorithm>
#include <string>
#include <vector>
#include "Element.h"
#include "Error.h"
using namespace std;

/*0 - whitespaces
1 - numbers
2 - kew_words and  identifiers
3 - one-symbol spliters
5 - comments
6 - unacceptable symbols
*/

//400 - 450 keywords
// 450 - 500 separators
// 500 - 1500 identifier

using namespace std;

class LexicalAnalyser: public ErrorList{
public:
	ifstream file;
	char c;
	list <pair<int, int>> Categories;
	list <pair<string, int>> KeyWordList;
	list <pair<string, int>> NumberList;
	list <pair<string, int>> ConstsList;
	list <pair<string, int>> SeparatorsList;
	list <pair<string, int>> IdentifierList;
	list <pair<string, int>> MoneyList;
private:

	//$
	enum Category { WhiteSpaces = 0, Numbers = 1, KeyWords = 2, Separator = 3, Comments = 5, UnacceptableValue = 6, Money = 7 };

	int row = 1;
	int column = 1;
	int countElemNumb = 0;
	int countElemIdentifier = 0;
	int countElemMoney= 0;

	map <char, int> ascii = {};
	map <string, int> CONSTS = {};
	map <string, int> keyWords = { {"var", 402}, {"begin", 403}, {"end", 404}, {"procedure", 405},
								{"if", 406 }, {"else", 407}, {"const", 408 }, {"integer", 409 }, {"float", 410 } };
	//{"(", 453}, {")", 454},
	map<string, int> Separators = { {"=", 450}, { ";", 452},  {",", 455}, {":", 456},{"-", 457} };
	map<string, int> CONST = { {"pi", 2000} };
	vector<Element> resultTable;

public:
	LexicalAnalyser();

	void fileReader(string filename);
	void ascciMaker();
	pair <int, int> categorisation(char symbol);
	
	int getNumberOfElement(char s);
	void incrementRow();
	void printKeyWordList();
	vector<Element> getResultTable();
	void setResultTable(vector<Element> table);
	int getRow();
	void setRow(int rw);
	int getColumn();
	void setColumn(int clmn);
	pair<int, int> caseWhitespaces(string tempStr, pair<int, int> tempPair);
	void caseNumbers(string tempStr, pair<int, int> tempPair);
	void caseKeyWords(string tempStr, pair<int, int> tempPair);
	void caseComments(string tempStr, pair<int, int> tempPair);
	void caseSeparator(string tempStr, pair<int, int> tempPair);
	void caseUnacceptableValue(string tempStr, pair<int, int> tempPair);
	void printListToFile(ofstream& file);
	void printResultTable() {
		for (auto i : resultTable) {
			i.printElement();
		}
	}
	void printResultTableToFile(ofstream& file) {
		for (auto i : resultTable) {
			i.printElementToFile(file);
		}
	}
};

