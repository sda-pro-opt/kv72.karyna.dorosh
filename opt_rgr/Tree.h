#pragma once
#include "Element.h"
#include <vector>

class TreeNode
{
public:
	TreeNode();
public:
	vector<TreeNode*> children;
	string rule;
	string token;
};

class Tree
{
public:
	TreeNode* root;

public:
	Tree();
	void insert(TreeNode*, TreeNode*);
	/*void showDictionary();
	void addWord();
	TreeNode* deleteWord(TreeNode*, string);
	Tree(string);*/
	Tree(vector<TreeNode*> nodes);

};