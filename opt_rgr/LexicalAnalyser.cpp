#include "LexicalAnalyser.h"

LexicalAnalyser::LexicalAnalyser() {
	ErrorList();
}
	void LexicalAnalyser::fileReader(string filename) {
		file.open(filename, ifstream::in);
		c = file.get();
		string tempStr = "";
		pair<int, int> tempPair;
		if (!file.is_open()) {
			cout << "Not found file\n";
			return;
		}
		while (!file.eof()) {
			tempStr = "";
			tempPair = this->categorisation(c);
			Categories.push_back(tempPair);

			if (tempPair.second == WhiteSpaces) {
				tempStr += c;
				tempPair = caseWhitespaces(tempStr, tempPair);
			}
			if (tempPair.second == Numbers) {
				caseNumbers(tempStr, tempPair);
			}
			if (tempPair.second == KeyWords) {
				caseKeyWords(tempStr, tempPair);
			}
			if (tempPair.second == Separator) {
				caseSeparator(tempStr, tempPair);
			}
			if (tempPair.second == Comments) {
				tempStr += c;
				caseComments(tempStr, tempPair);
			}
			if (tempPair.second == UnacceptableValue) {
				caseUnacceptableValue(tempStr, tempPair);
			}
			}
	}

	void LexicalAnalyser::ascciMaker() {
		int letter = 0;
		while (letter < 128) {
			this->ascii.insert(make_pair(char(letter), letter));
			letter++;
		}
	}

	pair<int, int> LexicalAnalyser::categorisation(char s) {
		int AsciiNumber = getNumberOfElement(s);
		pair<int, int> resultMap = {};
		// whitespaces: space, backspace,tab, new line, carriage return
		if ((AsciiNumber == 32) || (AsciiNumber == 8) || (AsciiNumber == 9) || (AsciiNumber == 10) || (AsciiNumber == 13) || (AsciiNumber == 0)) {
			resultMap = make_pair(AsciiNumber, 0);
		}
		// numbers
		else if ((AsciiNumber >= 48) && (AsciiNumber <= 57)) {
			resultMap = make_pair(AsciiNumber, 1);
		}
		//key_words or identifier
		else if (((AsciiNumber >= 65) && (AsciiNumber <= 90)) || ((AsciiNumber >= 97) && (AsciiNumber <= 122))) {
			resultMap = make_pair(AsciiNumber, 2);
		}
		//				:						;						=						,
		else if ((AsciiNumber == 58) || (AsciiNumber == 59) || (AsciiNumber == 61) || (AsciiNumber == 44) || (AsciiNumber == 45)) {
			resultMap = make_pair(AsciiNumber, 3);
		}
		else if ((AsciiNumber == 40) || (AsciiNumber == 41) || (AsciiNumber == 42)) {
			resultMap = make_pair(AsciiNumber, 5);
		}
		else {
			resultMap = make_pair(AsciiNumber, 6);
		}
		return resultMap;
	}

	int LexicalAnalyser::getNumberOfElement(char s) {
		for (auto i : ascii) {
			if (i.first == s)
				return i.second;
		}
		return -1;
	}

	void LexicalAnalyser::printKeyWordList() {
		cout << "\n  Keywords: " << endl;
		for (auto it : KeyWordList) {
			cout << " " << it.first << " " << it.second << endl;
		}
		cout << "\n  Identifiers:\n " << endl;
		for (auto it : IdentifierList) {
			cout << " " << it.first << " " << it.second << endl;
		}
		cout << "\n  Numbers:\n " << endl;
		for (auto it : NumberList) {
			cout << " " << it.first << " " << it.second << endl;
		}
		cout << "\n  Separators: " << endl;
		for (auto it : SeparatorsList) {
			cout << " " << it.first << " " << it.second << endl;
		}
	}
	void LexicalAnalyser::printListToFile(ofstream& file) {
		file << "\n  Keywords: " << endl;
		for (auto it : KeyWordList) {
			file << " " << it.first << " " << it.second << endl;
		}
		file << "\n  Identifiers:\n " << endl;
		for (auto it : IdentifierList) {
			file << " " << it.first << " " << it.second << endl;
		}
		file << "\n  Numbers:\n " << endl;
		for (auto it : NumberList) {
			file << " " << it.first << " " << it.second << endl;
		}
		file << "\n  Separators: " << endl;
		for (auto it : SeparatorsList) {
			file << " " << it.first << " " << it.second << endl;
		}
	}
	int LexicalAnalyser::getRow() {
		return this->row;
	}
	void LexicalAnalyser::setRow(int rw) {
		this->row = rw;
	}

	int LexicalAnalyser::getColumn() {
		return this->column;
	}
	void LexicalAnalyser::setColumn(int clmn) {
		this->column = clmn;
	}

	void LexicalAnalyser::incrementRow() {
		setRow(getRow() + 1);
		setColumn(0);
	}

	pair<int, int> LexicalAnalyser::caseWhitespaces(string tempStr, pair<int, int> tempPair) {
		while (!file.eof()) {

			if (tempStr == "\n") {
				incrementRow();
			}
			///c 
			c = file.get();
			setColumn(getColumn() + 1);
			tempStr = c;
			tempPair = this->categorisation(c);
			if (tempPair.second != WhiteSpaces) {
				tempPair = make_pair(-1, -1);
				break;
			}
		}
		return tempPair;
	}

	void LexicalAnalyser::caseNumbers(string tempStr, pair<int, int> tempPair) {
		while (!file.eof()) {
			tempStr += c;
			c = file.get();
			setColumn(getColumn() + 1);
			tempPair = this->categorisation(c);
			if (tempPair.second != Numbers) {
				if (tempPair.second == KeyWords) {
					this->errList.push_back(Error("Error! Wrong identifier ", getRow(), getColumn()));
				//	cout << "Error! Wrong identifier " << getRow() << " " << getColumn() << endl;
				}
				bool flag = true;
				bool flagConst = false;
				int temp = 0;
				list <pair<string, int>>::iterator it;
				//checking whether it wasnt added previously
				for (auto it = CONST.begin(); it != CONST.end(); it++) {
					if ((*it).first == tempStr) {
						flagConst = true;
						temp = (*it).second;
					}
				}
				for (auto it = NumberList.begin(); it != NumberList.end(); it++) {
					if ((*it).first == tempStr) {
						flag = false;
						temp = (*it).second;
					}
				}
				if (flag) {
					//if it wasnt added then add and increment element
					NumberList.push_back(make_pair(tempStr, 2010 + countElemNumb));
					resultTable.push_back(Element(2010 + countElemNumb, getRow(), getColumn() - tempStr.length(), tempStr));
					countElemNumb++;
				}
				else {
					resultTable.push_back(Element(temp, getRow(), getColumn() - tempStr.length(), tempStr));

				}
				break;
			}
		}
		tempPair = make_pair(-1, -1);
	}

	void LexicalAnalyser::caseKeyWords(string tempStr, pair<int, int> tempPair) {
		while (!file.eof()) {

			// seperate identifier from keyword
			bool identifier = true;
			//to get all string
			tempStr += c;
			//looking forward without moving pointer
			c = file.get();
			setColumn(getColumn() + 1);
			tempPair = this->categorisation(c);
			// if it is keyword or number then moving pointer c = file.get();
			if ((tempPair.second != KeyWords) && (tempPair.second != Numbers)) {
				transform(tempStr.begin(), tempStr.end(), tempStr.begin(), ::tolower);
				bool flagConst = false;
				int temp = 0;
				//checking whether it wasnt added previously
				for (auto it = CONST.begin(); it != CONST.end(); it++) {
					if ((*it).first == tempStr) {
						flagConst = true;
						temp = (*it).second;
					}
				}
				//checking if it is keyword
				bool isInKeyWordList = false;
				for (auto it = keyWords.begin(); it != keyWords.end(); it++) {
					if ((*it).first == tempStr) {
						for (auto key_it = KeyWordList.begin(); key_it != KeyWordList.end(); key_it++) {
							if ((*key_it).first == tempStr) {
								isInKeyWordList = true;
							}
						}
						if (!isInKeyWordList) {
							KeyWordList.push_back(make_pair((*it).first, (*it).second));
						}
						resultTable.push_back(Element((*it).second, getRow(), getColumn() - tempStr.length(), tempStr));
						identifier = false;
						break;
					}
				}

				// if not keyword the identfier
				if (identifier && !flagConst) {
					bool iden = true;
					int number = 0;
					for (auto i : IdentifierList) {
						if (tempStr == i.first) {
							iden = false;
							number = i.second;
						}
					}
					if (iden) {
						IdentifierList.push_back(make_pair(tempStr, 500 + countElemIdentifier));
						resultTable.push_back(Element(500 + countElemIdentifier, getRow(), getColumn() - tempStr.length(), tempStr));
					}
					else {
						resultTable.push_back(Element(number, getRow(), getColumn() - tempStr.length(), tempStr));
					}
					countElemIdentifier++;
				}
				if (flagConst) {
					resultTable.push_back(Element(temp, getRow(), getColumn() - tempStr.length(), tempStr));
					NumberList.push_front(make_pair(tempStr, temp));

				}
				//if not keyword or number then breaking cycle while, not the main
				break;
			}
		}
		tempPair = make_pair(-1, -1);

	}

	void LexicalAnalyser::caseComments(string tempStr, pair<int, int> tempPair) {
		string strComment = "";
		while (!file.eof()) {
			bool comment = false;
			string check = ""; //to check if it is comment or separator
			c = file.get();
			setColumn(getColumn() + 1);

			strComment = c;
			if (strComment == "*") {
				comment = true; //comment error
				while (!file.eof()) {
					c = file.get();
					setColumn(getColumn() + 1);
					strComment = c;

					if (strComment == "\n") {//in case that in comment new line
						incrementRow();
					}
					strComment = c;
					if (strComment == "*") {
						while (strComment == "*") {
							setColumn(getColumn() + 1);
							char end = file.get();
							strComment = end;
							if (strComment == "\n") {
								incrementRow();
							}
						}
						if (strComment == ")") {
							comment = false;
							break;
						}
					}
				}
				if (comment) {
					this->errList.push_back(Error("Error!Comment wasn't ended ", getRow(), getColumn() - strComment.length()));
					//cout << "Error!Comment wasn't ended " << getRow() << " " << getColumn() - strComment.length() << endl;
				}
				break;
			}
			else {
				if (tempStr == "(") {
					SeparatorsList.push_back(make_pair(tempStr, 457));
					resultTable.push_back(Element(457, getRow(), getColumn() - tempStr.length(), tempStr));
				}
				if (tempStr == ")") {
					SeparatorsList.push_back(make_pair(tempStr, 458));
					resultTable.push_back(Element(458, getRow(), getColumn() - tempStr.length(), tempStr));
				}
				break;
			}
		}
	}

	void LexicalAnalyser::caseSeparator(string tempStr, pair<int, int> tempPair) {
		while (!file.eof()) {
			tempStr = c;
			c = file.get();
			setColumn(getColumn() + 1);
			tempPair = this->categorisation(c);
			bool isInSeparatorList = false;
			for (auto it = Separators.begin(); it != Separators.end(); it++) {
				if ((*it).first == tempStr) {
					for (auto sep_it = SeparatorsList.begin(); sep_it != SeparatorsList.end(); sep_it++) {
						if ((*sep_it).first == tempStr) {
							isInSeparatorList = true;
						}
					}
					if (!isInSeparatorList) {
						SeparatorsList.push_back(make_pair((*it).first, (*it).second));
					}
					resultTable.push_back(Element((*it).second, getRow(), getColumn() - tempStr.length(), tempStr));
					break;
				}
			}
			if (tempPair.second != Separator)
				break;
		}
	}

	void LexicalAnalyser::caseUnacceptableValue(string tempStr, pair<int, int> tempPair) {
		this->errList.push_back(Error("Error! Unacceptable value ", getRow(), getColumn()));

		//cout << "Error! Unacceptable value " << getRow() << " " << getColumn() << endl;
		while (!file.eof()) {
			c = file.get();
			setColumn(getColumn() + 1);
			tempPair = this->categorisation(c);
			if (tempPair.second != UnacceptableValue) break;
		}
	}

	vector<Element> LexicalAnalyser::getResultTable() {
		return this->resultTable;
	}

	void LexicalAnalyser::setResultTable(vector<Element> table) {
		resultTable = table;
	}
