#include "Error.h"
#include<fstream>
Error::Error(string _err, int _row, int _column) {
    stringError = _err;
    row = _row;
    column = _column;
}
void Error::setRow(int _row) {
    row = _row;
}

int Error::getRow() {
    return row;
}

void Error::setColumn(int _column) {
    column = _column;
}

int Error::getColumn() {
    return column;
}

void ErrorList::setError(bool error) {
    isError = error;
}

bool ErrorList::getError() {
    return isError;
}

void Error::setStringError(string _stringError) {
    stringError = _stringError;
}

string Error::getStringErorr() {
    return stringError;
}
ErrorList::ErrorList() {
    errList = {};
    isError = true;

}
bool ErrorList::PrintErrors() {
    if (!errList.empty()) {
        for (auto error : errList) {
            cout << error.getStringErorr() << " " << error.getRow() << " " << error.getColumn() << endl;
        }
        return true;
    }
    else {
        return false;
    }
}
bool ErrorList::PrintErrorsToFile(ofstream& file) {
    if (!errList.empty()) {
        for (auto error : errList) {
            file << error.getStringErorr() << " " << error.getRow() << " " << error.getColumn() << endl;
        }
        return true;
    }
    else {
        return false;
    }
}