#pragma once
#include <iostream>
#include <string>
using namespace std;
class Element
{
private:
	int lexicalCode;
	int rowNumber;
	int columnNumber;
    string value;

public:
    Element(int code, int row, int column, string value);

    void setLexicalCode(int code);
    int getLexicalCode();

    void setRowNumber(int row);
    int getRowNumber();

    void setColumnNumber(int column);
    int getColumnNumber();

    string getValue();
    void setValue(string column);
    bool operator==(string el);
    void printElementToFile(ofstream& file);
    void printElement();
};

