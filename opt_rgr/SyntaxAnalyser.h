#include "Tree.h"
#include "LexicalAnalyser.h"

class SyntaxAnalyser: public ErrorList
{
private:
	int curLine = 1;
	int currColumn = 1;
	bool isConst = false;
public:
	vector<Element> lexicalAnalyser;

	Tree syntaxTree;
public:
	Tree getSyntaxTree();
	SyntaxAnalyser();
	SyntaxAnalyser(LexicalAnalyser* lexer);
	void StartSyntaxAnalysis();
	void SignalProgram();
	void Procedure(TreeNode* parent);
	void ProcedureIdentifier(TreeNode* parent);
	void Identifier(TreeNode* parent);
	void ParamentrList(TreeNode* parent);
	void DeclarationList(TreeNode* parent);
	void PrintSyntaxTree(TreeNode* parent, int tab);
	void AddingToTree(TreeNode* parent, TreeNode* currNode);
	void Declaration(TreeNode* parent);
	void VariableIdentifier(TreeNode* parent);
	void Attribute(TreeNode* parent);
	void Block(TreeNode* parent);
	void Declarations(TreeNode* parent);
	void StatementList(TreeNode* parent);
	void ConstantDeclarations(TreeNode* parent);
	void ConstantDeclarationsList(TreeNode* parent);
	void ConstantDeclaration(TreeNode* parent);
	void ConstantIdentifier(TreeNode* parent);
	void Constant(TreeNode* parent);
	void UnsignedInteger(TreeNode* parent);
	void PrintSyntaxTreeToFile(TreeNode* parent, int tab, ofstream& file);
	void SetCurLineAndColumn();
};

