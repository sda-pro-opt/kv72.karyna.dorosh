#include "Element.h"
#include <fstream>
Element::Element(int code, int row, int column, string value) {
    setLexicalCode(code);
    setColumnNumber(column);
    setRowNumber(row);
    setValue(value);
}

void Element::setLexicalCode(int code) {
    lexicalCode = code;
}

int Element::getLexicalCode() {
    return lexicalCode;
}

void Element::setRowNumber(int row) {
    rowNumber = row;
}

int Element::getRowNumber() {
    return rowNumber;
}

void Element::setColumnNumber(int column) {
    columnNumber = column;
}

int Element::getColumnNumber() {
    return columnNumber;
}

void Element::printElement() {
    std::cout << " " << getLexicalCode() << "  " << getRowNumber() << "  " << getColumnNumber() << " " << getValue() << std::endl;
}
void Element::printElementToFile(ofstream& file) {
    file << " " << getLexicalCode() << "  " << getRowNumber() << "  " << getColumnNumber() << " " << getValue() << std::endl;
}

string Element::getValue() {
    return value;
}

void Element::setValue(string column) {
    value = column;
}
bool Element::operator==(string el) {
    return getValue() == el;
}