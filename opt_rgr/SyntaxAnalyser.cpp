#include "SyntaxAnalyser.h"
#include <string>
Tree SyntaxAnalyser::getSyntaxTree() {
	return syntaxTree;
}

SyntaxAnalyser::SyntaxAnalyser() {
	syntaxTree.root = new TreeNode();
	curLine = 1;

}
SyntaxAnalyser::SyntaxAnalyser(LexicalAnalyser* lexer) {
	lexicalAnalyser = (lexer->getResultTable());
	syntaxTree.root = new TreeNode();
	curLine = 1;
}

void SyntaxAnalyser::StartSyntaxAnalysis() {
	SignalProgram();
}

void SyntaxAnalyser::SignalProgram() {
	syntaxTree.root->rule = "<signal-program>";
	Procedure(syntaxTree.root);
}
void SyntaxAnalyser::Procedure(TreeNode*parent) {
	TreeNode *currNode = new TreeNode();
	TreeNode *currNode2 = new TreeNode();
	currNode->rule = "<program>";
	syntaxTree.insert(parent, currNode);
	curLine = lexicalAnalyser[0].getRowNumber();
	if ((lexicalAnalyser.size() != 0) && (lexicalAnalyser[0].getValue() == "procedure") &&(this->errList.size() == 0)) {
		AddingToTree(currNode, currNode2);
	}
	else {
		this->errList.push_back(Error("Expected procedure on ", curLine, currColumn));
		return;
	}
	ProcedureIdentifier(currNode);

	ParamentrList(currNode);
	if (lexicalAnalyser.size() != 0 && lexicalAnalyser[0].getValue() == ";") {
		TreeNode* currNode2 = new TreeNode();
		AddingToTree(currNode, currNode2);
	}
	else if(this->errList.empty()) {
		this->errList.push_back(Error("Expected ; at line ", curLine, currColumn));
		return;
	}
	Block(currNode);
	if (lexicalAnalyser.size() != 0 && lexicalAnalyser[0].getValue() == ";") {
		TreeNode* end = new TreeNode();
		AddingToTree(currNode, end);
	}
	else if (this->errList.empty()) {
		this->errList.push_back(Error("Expected ; at line ", curLine+1, 0));
	}
	if (lexicalAnalyser.size() != 0 && errList.empty()) {
		curLine = lexicalAnalyser[0].getRowNumber();
		this->errList.push_back(Error("Unresolved symbols at line ", curLine, currColumn));
		return;
	}
}
void SyntaxAnalyser::ProcedureIdentifier(TreeNode* parent) {
	if (this->errList.size() == 0) {
		TreeNode* currNode = new TreeNode();
		currNode->rule = "<procedure-identifier>";
		syntaxTree.insert(parent, currNode);
		Identifier(currNode);
	}
}

void SyntaxAnalyser::Identifier(TreeNode* parent) {

	TreeNode* currNode = new TreeNode();
	currNode->rule = "<identifier>";
	if ((lexicalAnalyser.size() != 0) && (this->errList.size() == 0) && (((lexicalAnalyser[0].getLexicalCode() >= 500) && (lexicalAnalyser[0].getLexicalCode() <= 1500)))) {
		AddingToTree(parent, currNode);
	}
	else {
		this->errList.push_back(Error("Expected  identifier at line ", curLine, currColumn));
		return;
	}
}
void SyntaxAnalyser::ParamentrList(TreeNode *parent) {
	TreeNode* currNode = new TreeNode();
	TreeNode* currNode2 = new TreeNode();
	TreeNode* currNode3 = new TreeNode();

	if (!this->errList.empty()) return;
	currNode->rule = "<par-list>";
	syntaxTree.insert(parent, currNode);

	if ((lexicalAnalyser.size() != 0) && (lexicalAnalyser[0].getValue() == "(")) {
		AddingToTree(currNode, currNode3);

		DeclarationList(currNode);
		if ((lexicalAnalyser.size() != 0) && (lexicalAnalyser[0].getValue() == ")")) {
			curLine = lexicalAnalyser[0].getRowNumber();
			AddingToTree(currNode, currNode2);
		}
		else  if (this->errList.empty()) {
			this->errList.push_back(Error("Expected ; at line ", curLine, currColumn));
			return;
		}
	}
	else {
		currNode2->rule = "<empty>";
		syntaxTree.insert(currNode, currNode2);
		return;
	}
	
	
}
void SyntaxAnalyser::AddingToTree(TreeNode* parent, TreeNode *currNode) {
	currNode->token = to_string(lexicalAnalyser[0].getLexicalCode());
	auto temp = lexicalAnalyser.begin();
	auto del =  lexicalAnalyser.erase(lexicalAnalyser.begin());
	syntaxTree.insert(parent, currNode);
	SetCurLineAndColumn();
}
void SyntaxAnalyser::DeclarationList(TreeNode* parent) {
	if (!this->errList.empty()) return;

	TreeNode* currNode = new TreeNode();
	currNode->rule = "<declarations-list>";
	bool isEmpty = true;
	if ((lexicalAnalyser.size() != 0) && ((lexicalAnalyser[0].getLexicalCode() > 500) && (lexicalAnalyser[0].getLexicalCode() < 1500))) {
		isEmpty = false;
		syntaxTree.insert(parent, currNode);
		//AddingToTree(parent, currNode);	
		Declaration(currNode);
	}
	if (isEmpty) {
		syntaxTree.insert(parent, currNode);
		TreeNode* currNode2 = new TreeNode();
		currNode2->rule = "<empty>";
		syntaxTree.insert(currNode, currNode2);
		return;
	}
	DeclarationList(currNode);
	
}

void SyntaxAnalyser::PrintSyntaxTree(TreeNode *parent, int tab) {
	for (int i = 0; i <= tab; i++) {
		cout << "\t";
	}
	cout   << parent->rule << " " << parent->token << endl; 
	int i = 0;
	while (i < parent->children.size()) {
		
		PrintSyntaxTree(parent->children[i], tab+1);
		i++;
	}
}
void SyntaxAnalyser::PrintSyntaxTreeToFile(TreeNode* parent, int tab, ofstream& file) {
	for (int i = 0; i <= tab; i++) {
		file << "\t";
	}
	file << parent->rule << " " << parent->token << endl;
	int i = 0;
	while (i < parent->children.size()) {

		PrintSyntaxTreeToFile(parent->children[i], tab + 1, file);
		i++;
	}
}


void SyntaxAnalyser::Declaration(TreeNode* parent) {
	if (!this->errList.empty()) return;


	TreeNode* currNode = new TreeNode();
	currNode->rule = "<declaration>";
	syntaxTree.insert(parent, currNode);
	VariableIdentifier(currNode);
	if (lexicalAnalyser[0].getValue() == ":") {
		TreeNode* currNode2 = new TreeNode();
		//currNode2->token = to_string(lexicalAnalyser[0].getLexicalCode());
		AddingToTree(currNode, currNode2);
		Attribute(currNode);
		if (lexicalAnalyser[0].getValue() == ";") {
			TreeNode* currNode3 = new TreeNode();
			
			AddingToTree(currNode, currNode3);
		}
		else if (this->errList.empty()) {
			this->errList.push_back(Error("Expected ; at line ", curLine, currColumn));
			return;
		}
	}
	else {
		this->errList.push_back(Error("Expected : at line", curLine, currColumn));
		return;
	}
}
void SyntaxAnalyser::VariableIdentifier(TreeNode* parent) {
	if (!this->errList.empty()) return;

	TreeNode* currNode = new TreeNode();
	currNode->rule = "<variable-identifier>";
	syntaxTree.insert(parent, currNode);
	Identifier(currNode);
}
void SyntaxAnalyser::Attribute(TreeNode* parent) {
	if (!this->errList.empty()) return;

	TreeNode* currNode = new TreeNode();
	currNode->rule = "<attribute>";
	if ((lexicalAnalyser[0].getValue() == "integer") || (lexicalAnalyser[0].getValue() == "float")) {
		AddingToTree(parent, currNode);
	}
	else{
		this->errList.push_back(Error("Expected attribute at line", curLine, currColumn));

		return;
	}
}
void SyntaxAnalyser::Block(TreeNode* parent) {
	if (!this->errList.empty()) return;

	TreeNode* currNode = new TreeNode();
	currNode->rule = "<block>";
	syntaxTree.insert(parent, currNode);
	Declarations(currNode);
	if (lexicalAnalyser.size() != 0 && lexicalAnalyser[0].getValue() == "begin" && this->errList.empty()) {
		TreeNode* currNode2 = new TreeNode();

		AddingToTree(currNode, currNode2);
		StatementList(currNode);
		
		if (lexicalAnalyser.size() != 0 && lexicalAnalyser[0].getValue() == "end") {
			TreeNode* currNode3 = new TreeNode();
			AddingToTree(currNode, currNode3);
		}
		else {
			this->errList.push_back(Error("Expected end at line", curLine+1, 0));
			return;
		}
	}
	else if (this->errList.empty()) {
		this->errList.push_back(Error("Expected begin at line", curLine, currColumn));

		return;
	}
}
void SyntaxAnalyser::SetCurLineAndColumn() {
	if (lexicalAnalyser.size() != 0) {
		curLine = lexicalAnalyser[0].getRowNumber();
		currColumn = lexicalAnalyser[0].getColumnNumber();
	}
	else {
		currColumn++;
	}
}

void SyntaxAnalyser::Declarations(TreeNode* parent) {
	if (!this->errList.empty()) return;

	TreeNode* currNode = new TreeNode();
	currNode->rule = "<declarations>";
	syntaxTree.insert(parent, currNode);
	ConstantDeclarations(currNode);

}
void SyntaxAnalyser::StatementList(TreeNode* parent) {
	if (!this->errList.empty()) return;

	TreeNode* currNode = new TreeNode();
	currNode->rule = "<statement-list>";
	syntaxTree.insert(parent, currNode);
	TreeNode* currNode2 = new TreeNode();
	currNode2->rule = "<empty>";
	syntaxTree.insert(currNode, currNode2);
	return;

}
void SyntaxAnalyser::ConstantDeclarations(TreeNode* parent) {
	if (!this->errList.empty()) return;

	TreeNode* currNode = new TreeNode();
	currNode->rule = "<constant-declarations>";
	syntaxTree.insert(parent, currNode);
	if (lexicalAnalyser[0].getValue() == "const") {
		TreeNode* currNode2 = new TreeNode();
		AddingToTree(currNode, currNode2);
		
		ConstantDeclarationsList(currNode);

	}
	else {
		TreeNode* currNode2 = new TreeNode();
		currNode2->rule = "<empty>";
		syntaxTree.insert(currNode, currNode2);

		return;
	}
}
void SyntaxAnalyser::ConstantDeclarationsList(TreeNode* parent) {
	if (!this->errList.empty()) return;

	TreeNode* currNode = new TreeNode();
	currNode->rule = "<constant-declarations-list>";
	syntaxTree.insert(parent, currNode);
	if ((lexicalAnalyser.size() != 0 )&& ((lexicalAnalyser[0].getLexicalCode() >= 500) && (lexicalAnalyser[0].getLexicalCode() <= 1500))) {
		isConst = true;
		ConstantDeclaration(currNode);
		ConstantDeclarationsList(currNode);
	}
	else {
		TreeNode* currNode2 = new TreeNode();
		currNode2->rule = "<empty>";
		syntaxTree.insert(currNode, currNode2);
	}
}
void SyntaxAnalyser::ConstantDeclaration(TreeNode* parent) {
	if (!this->errList.empty()) return;

	TreeNode* currNode = new TreeNode();
	currNode->rule = "<constant-declaration>";
	syntaxTree.insert(parent, currNode);
	ConstantIdentifier(currNode);
	if (lexicalAnalyser[0].getValue() == "=") {
		TreeNode* currNode2 = new TreeNode();
		AddingToTree(currNode, currNode2);
		Constant(currNode);
	}
	else if (this->errList.empty()) {
		this->errList.push_back(Error("Expected = at line", curLine, currColumn));

		return;
	}
	if (lexicalAnalyser[0].getValue() == ";") {
		TreeNode* currNode2 = new TreeNode();
		AddingToTree(currNode, currNode2);
	}
	else if (this->errList.empty()) {
		this->errList.push_back(Error("Expected ; at line", curLine, currColumn));
		return;
	}
}
void SyntaxAnalyser::ConstantIdentifier(TreeNode* parent) {
	if (!this->errList.empty()) return;

	TreeNode* currNode = new TreeNode();
	currNode->rule = "<constant-identifier>";
	syntaxTree.insert(parent, currNode);
	Identifier(currNode);
}
void SyntaxAnalyser::Constant(TreeNode* parent) {
	if (!this->errList.empty()) return;

	TreeNode* currNode = new TreeNode();
	currNode->rule = "<constant>";
	syntaxTree.insert(parent, currNode);
	if (lexicalAnalyser[0].getValue() == "-") {
		TreeNode* currNode2 = new TreeNode();
		AddingToTree(currNode, currNode2);
		UnsignedInteger(currNode2);
	}
	else {
		UnsignedInteger(currNode);
	}

}
void SyntaxAnalyser::UnsignedInteger(TreeNode* parent) {
	if (!this->errList.empty()) return;

	TreeNode* currNode = new TreeNode();
	currNode->rule = "<unsigned-integer>";
	if (lexicalAnalyser.size() != 0 && lexicalAnalyser[0].getLexicalCode() >= 2010) {
		AddingToTree(parent, currNode);
	}
	else if (this->errList.empty()) {
		this->errList.push_back(Error("Expected number at line", curLine, currColumn));
		return;
	}
}
