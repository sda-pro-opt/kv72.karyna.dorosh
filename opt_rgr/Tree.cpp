#include "Tree.h"

TreeNode::TreeNode(){

	rule = "";
	token = "";
}

Tree::Tree(){
	root = nullptr;
}

void Tree::insert(TreeNode* rt, TreeNode* node) {
	rt->children.push_back(node);
}

Tree::Tree(vector<TreeNode*> nodes) {
	root = nodes[0];
	nodes.erase(nodes.begin());

	vector<TreeNode*>::iterator it;
	for (it = nodes.begin(); it != nodes.end(); it++) {
		insert(root, *it);
	}
}