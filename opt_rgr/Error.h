#include "Element.h"
#include <vector>
class Error
{
private:
	string stringError;
	int row;
	int column;
public:
	Error(string, int, int);
	void setRow(int row);
	int getRow();


	void setStringError(string _stringError);
	string getStringErorr();

	void setColumn(int row);
	int getColumn();

};

class ErrorList {
private:
	bool isError;

public:
	//ErrorList(Error err);
	ErrorList();

	vector<Error> errList;
	bool PrintErrors();
	bool PrintErrorsToFile(ofstream&file);

	void setError(bool row);
	bool getError();
};