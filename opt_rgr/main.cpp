#include "SyntaxAnalyser.h"
int main() {
	system("color F0");
	const char* filename = "test_main.signal";
	LexicalAnalyser *obj = new LexicalAnalyser();
	obj->ascciMaker();
	obj->fileReader(filename);

	obj->printResultTable();
	obj->printKeyWordList();
	SyntaxAnalyser* syntaxAnalyser = new SyntaxAnalyser(obj);

	if (obj->errList.size() == 0) {
		syntaxAnalyser->StartSyntaxAnalysis();
		syntaxAnalyser->PrintSyntaxTree(syntaxAnalyser->syntaxTree.root, 0);
		syntaxAnalyser->PrintErrors();
	}
	obj->PrintErrors();
	ofstream file;
	file.open("generated.txt", ofstream::out);
	if (file.is_open()) {

		obj->printResultTableToFile(file);
		obj->printListToFile(file);
		obj->PrintErrorsToFile(file);
		syntaxAnalyser->PrintSyntaxTreeToFile(syntaxAnalyser->syntaxTree.root, 0, file);
		syntaxAnalyser->PrintErrorsToFile(file);

	}
	else {
		cout << "Can't open file" << endl;
	}
	return 0;
}